#ifndef F50SSIMULATOR_H
#define F50SSIMULATOR_H

#include <QMainWindow>
#include "F50S.hpp"

namespace Ui {
class F50SSimulator;
}

class F50SSimulator : public QMainWindow {
    Q_OBJECT

public:
    explicit F50SSimulator(QWidget *parent = 0);
    ~F50SSimulator();

private slots:
    void on_HeTempPreH20_valueChanged(double arg1);

    void on_HeTempAftH20_valueChanged(double arg1);

    void on_OilTempPreH20_valueChanged(double arg1);

    void on_OilTempAftH20_valueChanged(double arg1);

    void on_WaterTempInlet_valueChanged(double arg1);

    void on_WaterTempOutlet_valueChanged(double arg1);

    void on_HePresSupply_valueChanged(double arg1);

    void on_HePresReturn_valueChanged(double arg1);

    void on_CurrentCHFreq_valueChanged(double arg1);

    void on_F50SMode_activated(int index);

    void on_addAlarm_activated(const QString &arg1);

    void on_delAlarm_activated(const QString &arg1);

    void on_pushButton_clicked(bool checked);


private:
    Ui::F50SSimulator *ui;
    std::thread mUpdateUIThread;
    std::atomic<bool> killUpdateUIThread = { false };
    std::atomic<bool> IsConfigured = { false };
    void updateUIThread(void);

    };

#endif // F50SSIMULATOR_H
