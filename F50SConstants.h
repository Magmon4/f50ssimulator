#ifndef F50SCONSTANTS_H
#define F50SCONSTANTS_H
const char dataDelim = ',';

enum class F50SMode {
    INTERNAL = 1,
    D_SUB15,
    RS232
};

typedef enum {
    eCmprSHeTempPreH20,
    eCmprSHeTempAftH20,
    eCmprSOilTempPreH20,
    eCmprSOilTempAftH20,
    eCmprsSWaterTempInlet,
    eCmprsSWaterTempOutlet
} F50STemp;

typedef enum {
    eCmprSHePresSupply,
    eCmprSHePresReturn
} F50SPres;

typedef enum {
    BeforeInit,
    Init,
    StopByInitByAlarm,
    Stop,
    PrepareRun,
    PrepareCHOnlyRun,
    Run,
    CHOnlyRun,
    PrepareStop,
    PrepareCHOnlyStop,
    FaultOffByAlarm,
    AlarmReset,
    ProcToOprMode,
    Max_Status
} F50SStatusCode;

typedef enum {
    AllTempVal ,
    HeTempPreH20, HeTempAftH20, OilTempPreH20, OilTempAftH20, WaterTempInlet, WaterTempOutlet,
    AllPresVal,
    HePressureSupply, HePressureReturn,
    TotalRunTime, AdsorberUsageTime, AdsorberRemainderTime,
    SoftVersion, SysStructFileClass, AbnormalStopHistory, WarnHistory,
    TurnOnF50S, TurnOffF50S, ResetAlarm, OnIndCHOpr, OffIndCHOpr,
    CurrCHOprFreq, SetCHOprFreq, ServiceTimer, CurStatusCode,
    PresAlarmInfoBegin, PresAlarmInfoCont,
    FailurePred1, FailurePred2, FailurePred3, FailurePred4, FailurePred5,
    Mode, ChangeOprModeToExt,
    EventLogNum, EventLogRec, IntervalLogNum, IntervalLogRec,
    ErrCRC,
    FwUpdateMode, ClearProgArea, WriteBlock, ResetCPU,
    maxCmd
} F50SCmd;

    enum class EventType {
       FinishInitAftMainPowerOn    = 01,
       ChangeOprModeToInternal     = 02,
       ChangeOprModeToExternal     = 03,
       StartRunning                = 11,
       StopRunning                 = 12,
       AlarmOccured                = 31,
       AlarmReset                  = 32,
       WarningOccured              = 41,
       WarningCleared              = 42
   };

   enum class  SubEventType {
       Always                      = 0,
       OperatedByRunStopSwitch     = 1,
       OperatedByDsub15Signal      = 2,
       OperatedByRS232CCmd         = 3
   };

   enum class FwUpdateErrorCode {
          Success                     = 0,
          RequestNotAccepted,
          AlimentError,
          ByteError,
          AddressError,
          BoundaryError,
          OperationError,
          SCodeError
   };

   enum class AlarmCode {
       A000 =   0,
       A001 =   1,
       A002 =   2,
       A003 =   3,
       A004 =   4,
       A006 =   6,
       A007 =   7,
       A008 =   8,
       A009 =   9,
       A101 =   101,
       A102 =   102,
       A103 =   103,
       A104 =   104,
       A106 =   106,
       A107 =   107,
       A108 =   108,
       A109 =   109,
       A116 =   116,
       A117 =   117,
       A118 =   118,
       A119 =   119,
       A121 =   121,
       A122 =   122,
       A123 =   123,
       A124 =   124,
       A146 =   146,
       A147 =   147,
       A148 =   148,
       A149 =   149,
       A151 =   151,
       A152 =   152,
       A153 =   153,
       A154 =   154,
       A165 =   165,
       A601 =   601,
       A603 =   603,
       A605 =   605,
       A300 =   300,
       A306 =   306,
       A307 =   307,
       A308 =   308,
       A309 =   309,
       A310 =   310,
       A311 =   311,
       A314 =   314,
       A315 =   315,
       A316 =   316,
       A317 =   317,
       A318 =   318,
       A319 =   319
   };

   enum class WarnCode {
       W000 =   0,
       W001 =   1,
       W002 =   2,
       W006 =   6,
       W007 =   7,
       W101 =   101,
       W102 =   102,
       W106 =   106,
       W107 =   107,
       W116 =   116,
       W117 =   117,
       W121 =   121,
       W122 =   122,
       W146 =   146,
       W147 =   147,
       W151 =   151,
       W152 =   152,
       W310 =   310,
       W311 =   311,
       W312 =   312,
       W313 =   313,
       W314 =   314,
       W315 =   315,
       W402 =   402
   };

#endif // F50SCONSTANTS_H

