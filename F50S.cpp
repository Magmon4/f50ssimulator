/**
 * @brief
 *
 * @file F50S.cpp
 * @author Vipin Kumar
 * @date 2018-08-07
 */

#include "F50S.hpp"

void F50S::rxThread () {
    int n = 0;
    std::cout << "In Thread!!" << std::endl;
    UARTIfaceSingleton::getInstance()->InitUARTComm(UART_DEV_FILE,
                                                    BaudRate::BAUD_9600,
                                                    CharacterSize::CHAR_SIZE_8,
                                                    Parity::PARITY_NONE,
                                                    FlowControl::FLOW_CONTROL_NONE,
                                                    StopBits::STOP_BITS_1);

    setCurrStatusCode(F50SStatusCodeStr[BeforeInit]);
    std::cout << "Waiting" << std::endl;
    sleep(STATUS_CHANGE_INTERVAL);
    setCurrStatusCode(F50SStatusCodeStr[Init]);
    sleep(STATUS_CHANGE_INTERVAL);
    logEvent(EventType::FinishInitAftMainPowerOn, SubEventType::Always,
             AlarmCode::A000, WarnCode::W000);
    setCurrStatusCode(F50SStatusCodeStr[Stop]);
    pF50SData.powerStatus = false;
    bool paramUpdated = false;
    while(!mKillRxThread) {
        printf("\nWaiting for magmon...\n");
        //Clear the buffer
        recvDataBuff.clear();

        try {
            //! Wait until data available, No Timeout
            UARTIfaceSingleton::getInstance()->waitUntilDataAvailable(0);
            UARTIfaceSingleton::getInstance()->readPort(recvDataBuff);
            n = recvDataBuff.size();
//            printf("\033[36m--- RX %ld bytes ---\033[0m\n", n);

//            dumpFrame(&recvDataBuff[0], n);
        } catch (...) {
            std::cout << "MM4 is not responding" << std::endl;
        }
        try {
            std::cout << "acceptFrame called" << std::endl;
            acceptFrame(&recvDataBuff[0], (size_t) n);
            std::cout << "acceptFrameComp" << std::endl;
        } catch (CRCMismatch) {
            respondErrCRC();
        }
//        if(isFwUpdateModeActivated && !paramUpdated) {
//            // Updating UART Params in Firmware update mode
//            std::cout << "FwMode Activated, Updating param" << std::endl;
//            UARTIfaceSingleton::getInstance()->updateUARTParams(BaudRate::BAUD_115200,
//                                                                CharacterSize::CHAR_SIZE_8,
//                                                                StopBits::STOP_BITS_2);
//            paramUpdated = true;
//        } else if(isFwUpdateCompleted) {
//            std::cout << "Fw Update completed, Resetting the params" << std::endl;
//            UARTIfaceSingleton::getInstance()->updateUARTParams(BaudRate::BAUD_9600,
//                                                                CharacterSize::CHAR_SIZE_8,
//                                                                StopBits::STOP_BITS_1);
//            isFwUpdateCompleted = false;
//            paramUpdated = false;
//        }
    }
}

void F50S::CheckThread () {
    IntervalLog intervalLog;
    uint32_t count = 0;
    //! Compressor power status should be off at Initialization
    pF50SData.powerStatus = false;
    //! Initialize Coldhead Freq;
    setColdHeadFreq(30.0);
    pF50SData.totalRunTime = 0;
    setIndColdHeadOperation(false);
    //! Set F50S mode to RS232 at startup
    setF50SMode(F50SMode::INTERNAL);

    for(auto i = 0 ; i < MAX_ALARM_FROM_BEGIN + MAX_ALARM_FROM_CONT; i++) {
        pF50SData.alarmInfo[i] = AlarmCode::A000;
    }

    //! Initialize Temperature/Pressure values
    setTemp(eCmprSHeTempPreH20, 0.0);
    setTemp(eCmprSHeTempAftH20, 0.0);
    setTemp(eCmprSOilTempPreH20, 0.0);
    setTemp(eCmprSOilTempAftH20, 0.0);
    setTemp(eCmprsSWaterTempInlet, 0.0);
    setTemp(eCmprsSWaterTempOutlet, 0.0);
    setPres(eCmprSHePresSupply, 0.0);
    setPres(eCmprSHePresReturn, 0.0);


    //ToDo: Need to handle subevent type
    SubEventType subEventType = SubEventType::OperatedByRS232CCmd;
    while(!mKillCheckThread) {
        auto nextTimePoint = std::chrono::steady_clock::now() + tickInterval();
        if(!isAlarmCleared) {
            //! Stopping compressor on any alarm occurrence
            std::cout << "Current Status: PrepareStop" << std::endl;
            if(pF50SData.powerStatus) {
                setCurrStatusCode((F50SStatusCodeStr[PrepareStop]));
                sleep(STATUS_CHANGE_INTERVAL);
                std::cout << "Logging event for Stop F50S\n";
                setCurrStatusCode(F50SStatusCodeStr[Stop]);
                //! What should the subevent type when compressor is stopped in case of alarms?
                stopF50S(SubEventType::Always);
            }
            setCurrStatusCode(F50SStatusCodeStr[FaultOffByAlarm]);
        } else {
            if (!pF50SData.currentStatusCode.compare(F50SStatusCodeStr[PrepareRun])) {
                sleep(STATUS_CHANGE_INTERVAL);
                std::cout << "CurrentMode: PrepareRun" << std::endl;

                setCurrStatusCode(F50SStatusCodeStr[Run]);
                pF50SData.powerStatus = true;
                pF50SData.totalRunTime++;
            } else if(!pF50SData.currentStatusCode.compare(F50SStatusCodeStr[Run])) {
//                    std::cout << "CurrentMode: Run" << std::endl;
                pF50SData.totalRunTime++;
            } else if(!pF50SData.currentStatusCode.compare(F50SStatusCodeStr[PrepareStop])) {
                sleep(STATUS_CHANGE_INTERVAL);
                setCurrStatusCode(F50SStatusCodeStr[Stop]);
            } else if(!pF50SData.currentStatusCode.compare(F50SStatusCodeStr[Stop])) {
//                    std::cout << "CurrentStatus: Stop" << std::endl;
            } else if(!pF50SData.currentStatusCode.compare(F50SStatusCodeStr[ProcToOprMode])) {
                sleep(STATUS_CHANGE_INTERVAL);
                std::cout << "CurrentMode: ProcToOprMode" << std::endl;
                setCurrStatusCode(F50SStatusCodeStr[Stop]);
            }
        }

        //! Fill the interval log queue with test data
        if(count++ % 600 == 0 ) {
            if(count > 600)
                count = 0;
            intervalLog.intLogTime = chrono::system_clock::to_time_t(chrono::system_clock::now());
            for(auto i = 0; i < TEMP_MAX; i++)
                intervalLog.temp[i] = pF50SData.temperature[i];

            intervalLog.pressure[0] = pF50SData.pressure[0];
            intervalLog.pressure[0] = pF50SData.pressure[0];
            intervalLog.noUse[0] = 0;
            intervalLog.coldHeadInverter = 60;
            intervalLog.controlValvePercent = 12;
            for(auto i = 0; i < 5; i++)
                intervalLog.failPredCriterion[i] =  intervalLog.intLogTime % 1000 + i;

            if(intervalLogQueue.size() > 11200)
                intervalLogQueue.pop();
            intervalLogQueue.push(intervalLog);
            std::cout << "Interval Logged: " << count << std::endl;
        }
        if (nextTimePoint < std::chrono::steady_clock::now()) {
        }
        else {
            std::this_thread::sleep_until(nextTimePoint);
        }
    }
}

AlarmCode F50S::getAlarmInfo(int index) {
    if(!(pF50SData.alarmInfo[index] == AlarmCode::A000))
        return pF50SData.alarmInfo[index];
    else
        return AlarmCode::A000;
}

void F50S::Initialization(void) {

    sfInit(&mContext);
    sfSetDeliverCallback(&mContext, staticDeliver, this);
    sfSetWriteToUARTCallback(&mContext, staticWriteToUART, this);
    sfSetLockCallback(&mContext, staticLock, this);
    sfSetUnlockCallback(&mContext, staticUnlock, this);
    pF50SData.softVersion = SOFT_VERSION;
}

void F50S::handleRxPacket(SFPacket &packet) {
    //switch Cases
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    std::cout << "Cmd: " << cmd << std::endl;
    if(cmd.compare(F50SCmdStr[AllTempVal]) == 0)
        respondAllTemp(packet);
    else if(cmd.compare(F50SCmdStr[HeTempPreH20]) == 0)
        respondHeTempPreH2O(packet);
    else if(cmd.compare(F50SCmdStr[HeTempAftH20]) == 0)
        respondHeTempAftH2O(packet);
    else if(cmd.compare(F50SCmdStr[OilTempPreH20]) == 0)
        respondOilTempPreH2O(packet);
    else if(cmd.compare(F50SCmdStr[OilTempAftH20]) == 0)
        respondOilTempAftH2O(packet);
    else if(cmd.compare(F50SCmdStr[WaterTempInlet]) == 0)
        respondWaterTempInlet(packet);
    else if(cmd.compare(F50SCmdStr[WaterTempOutlet]) == 0)
        respondWaterTempOutlet(packet);
    else if(cmd.compare(F50SCmdStr[AllPresVal]) == 0)
        respondAllPres(packet);
    else if(cmd.compare(F50SCmdStr[CurStatusCode]) == 0)
        respondCurStatusCode(packet);
    else if(cmd.compare(F50SCmdStr[TurnOnF50S]) == 0)
        respondTurnOnF50S(packet);
    else if(cmd.compare(F50SCmdStr[TurnOffF50S]) == 0)
        respondTurnOffF50S(packet);
    else if(cmd.compare(F50SCmdStr[PresAlarmInfoBegin]) == 0)
        respondPresAlarmInfoBegin(packet);
    else if(cmd.compare(F50SCmdStr[PresAlarmInfoCont]) == 0)
        respondPresAlarmInfoCont(packet);
    else if(cmd.compare(F50SCmdStr[SoftVersion]) == 0)
        respondSoftVersion(packet);
    else if(cmd.compare(F50SCmdStr[SysStructFileClass]) == 0)
        respondSysStructFileClass(packet);
    else if(cmd.compare(F50SCmdStr[CurrCHOprFreq]) == 0)
        respondCurrCHOprFreq(packet);
    else if(cmd.compare(F50SCmdStr[SetCHOprFreq]) == 0)
        respondSetCHOprFreq(packet);
    else if(cmd.compare(F50SCmdStr[TotalRunTime]) == 0)
        respondTotalRunTime(packet);
    else if(cmd.compare(F50SCmdStr[AdsorberUsageTime]) == 0)
        respondAdsorberUsageTime(packet);
    else if(cmd.compare(F50SCmdStr[AdsorberRemainderTime]) == 0)
        respondAdsorberRemainderTime(packet);
    else if(cmd.compare(F50SCmdStr[ServiceTimer]) == 0)
        respondServiceTimer(packet);
    else if(cmd.compare(F50SCmdStr[AbnormalStopHistory]) == 0)
        respondAbnormalStopHistory(packet);
    else if(cmd.compare(F50SCmdStr[WarnHistory]) == 0)
        respondWarningHistory(packet);
    else if(cmd.compare(F50SCmdStr[OnIndCHOpr]) == 0)
        respondOnIndCHOpr(packet);
    else if(cmd.compare(F50SCmdStr[OffIndCHOpr]) == 0)
        respondOffIndCHOpr(packet);
    else if(cmd.compare(F50SCmdStr[Mode]) == 0)
        respondF50SMode(packet);
    else if(cmd.compare(F50SCmdStr[ResetAlarm]) == 0)
        respondResetAlarm(packet);
    else if(cmd.compare(F50SCmdStr[EventLogNum]) == 0)
        respondEventLogNum(packet);
    else if(cmd.compare(F50SCmdStr[EventLogRec]) == 0)
        respondEventRecord(packet);
    else if(cmd.compare(F50SCmdStr[IntervalLogNum]) == 0)
        respondIntervalLogNum(packet);
    else if(cmd.compare(F50SCmdStr[IntervalLogRec]) == 0)
        respondIntervalLogRecord(packet);
    else if(cmd.compare(F50SCmdStr[FwUpdateMode]) == 0)
        respondChangeFwMode(packet);
    else if(cmd.compare(F50SCmdStr[ClearProgArea]) == 0)
        respondClearProgMode(packet);
    else if(cmd.compare(F50SCmdStr[WriteBlock]) == 0)
        respondWriteBlock(packet);
    else if(cmd.compare(F50SCmdStr[ResetCPU]) == 0)
        respondResetCPU(packet);
    else if(cmd.compare(F50SCmdStr[FailurePred1]) == 0)
        respondFailurePredCriterion_1(packet);
    else if(cmd.compare(F50SCmdStr[FailurePred2]) == 0)
        respondFailurePredCriterion_2(packet);
    else if(cmd.compare(F50SCmdStr[FailurePred3]) == 0)
        respondFailurePredCriterion_3(packet);
    else if(cmd.compare(F50SCmdStr[FailurePred4]) == 0)
        respondFailurePredCriterion_4(packet);
    else if(cmd.compare(F50SCmdStr[FailurePred5]) == 0)
        respondFailurePredCriterion_5(packet);
    else if(cmd.compare(F50SCmdStr[ChangeOprModeToExt]) == 0)
        respondChangeModeToExt(packet);
    std::cout << "Handle completed" << std::endl;
}

void F50S::respondAllTemp(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%6.1f,%6.1f,%6.1f,%6.1f,%6.1f,%6.1f ",
    cmd.c_str(), getHeTempPreH20(), getHeTempAftH20(), getOilTempPreH20(),
     getOilTempAftH20(), getWaterTempInlet(), getWaterTempOutlet());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondHeTempPreH2O(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%6.1f,", cmd.c_str(), getHeTempPreH20());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondHeTempAftH2O(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%6.1f,", cmd.c_str(), getHeTempAftH20());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondOilTempPreH2O(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%6.1f,", cmd.c_str(), getOilTempPreH20());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondOilTempAftH2O(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%6.1f,", cmd.c_str(), getOilTempAftH20());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondWaterTempInlet(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%6.1f,", cmd.c_str(), getWaterTempInlet());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondWaterTempOutlet(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%6.1f,", cmd.c_str(), getWaterTempOutlet());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondAllPres(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%5.2f,%5.2f,",
    cmd.c_str(), getReturnPressure() , getSupplyPressure());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondCurStatusCode(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%s,", cmd.c_str(), getCurrStatusCode().c_str());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondTurnOnF50S(SFPacket &packet) {
    if(!pF50SData.powerStatus)
        startF50S(SubEventType::OperatedByRS232CCmd);
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,", cmd.c_str());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondTurnOffF50S(SFPacket &packet) {
    if(pF50SData.powerStatus)
        stopF50S(SubEventType::OperatedByRS232CCmd);
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,", cmd.c_str());
    sendMsg((uint8_t *) buff, strlen(buff));
}


void F50S::respondPresAlarmInfoBegin(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string outBuf;
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,", cmd.c_str());
    outBuf.append(buff);
    for(auto i = 0; i < MAX_ALARM_FROM_BEGIN; i++) {
        if(pF50SData.alarmInfo[i] !=  AlarmCode::A000) {
            sprintf(buff, "%3d,", pF50SData.alarmInfo[i]);
            outBuf.append(buff);
        }
    }
    sendMsg((uint8_t *) outBuf.c_str(), outBuf.size());
}

void F50S::respondPresAlarmInfoCont(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string outBuf;
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,", cmd.c_str());
    outBuf.append(buff);
    for(auto i = MAX_ALARM_FROM_BEGIN ; i < MAX_ALARM_FROM_BEGIN + MAX_ALARM_FROM_CONT; i++) {
        if(pF50SData.alarmInfo[i] != AlarmCode::A000) {
            sprintf(buff, "%3d,", pF50SData.alarmInfo[i]);
            outBuf.append(buff);
        }
    }
    sendMsg((uint8_t *) outBuf.c_str(), outBuf.size());
}

void F50S::respondSoftVersion(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%s,", cmd.c_str(), pF50SData.softVersion.c_str());
    sendMsg((uint8_t *) buff, strlen(buff));
}


void F50S::respondSysStructFileClass(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%s,", cmd.c_str(), "testClass");
    sendMsg((uint8_t *) buff, strlen(buff));
}
void F50S::respondCurrCHOprFreq(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%6.1f,", cmd.c_str(), getColdHeadFreq());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondFailurePredCriterion_1(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%6.1f,", cmd.c_str(), 1.00);
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondFailurePredCriterion_2(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%6.1f,", cmd.c_str(), 2.00);
    sendMsg((uint8_t *) buff, strlen(buff));
}
void F50S::respondFailurePredCriterion_3(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%6.1f,", cmd.c_str(), 3.00);
    sendMsg((uint8_t *) buff, strlen(buff));
}
void F50S::respondFailurePredCriterion_4(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%6.1f,", cmd.c_str(), 4.00);
    sendMsg((uint8_t *) buff, strlen(buff));
}
void F50S::respondFailurePredCriterion_5(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%6.1f,", cmd.c_str(), 5.00);
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondChangeModeToExt(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    setF50SMode(F50SMode::RS232);
    sprintf(buff, "%s,", cmd.c_str());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondSetCHOprFreq(SFPacket &packet) {
    string packetBuf = (char *)packet.buf;
    static char buff[RESP_BUFF_SIZE];
    string cmd = packetBuf.substr(0, F50S_CMD_LEN);
    string cmdData = packetBuf.substr(F50S_CMD_LEN + 1, packet.len);
    float freq = stof(cmdData);
    changeColdHeadFreq(freq);
    sprintf(buff, "%s,%6.1f,", cmd.c_str(), getColdHeadFreq());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondTotalRunTime(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%8.1f,", cmd.c_str(), getTotalRunTime());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondAdsorberUsageTime(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%7.1f,", cmd.c_str(), getTotalRunTime());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondAdsorberRemainderTime(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%7.1f,", cmd.c_str(), getTotalRunTime());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondServiceTimer(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%7.1f,", cmd.c_str(), getTotalRunTime());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondAbnormalStopHistory(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s%hx,", cmd.c_str(), 5);
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondWarningHistory(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s%hx,", cmd.c_str(), 6);
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondOnIndCHOpr(SFPacket &packet) {
    setIndColdHeadOperation(true);
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,", cmd.c_str());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondOffIndCHOpr(SFPacket &packet) {
    setIndColdHeadOperation(false);
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,", cmd.c_str());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondF50SMode(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%d,", cmd.c_str(), (int)getF50SMode());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondResetAlarm(SFPacket &packet) {
    resetAlarms();
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,", cmd.c_str());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondEventLogNum(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%d,", cmd.c_str(), eventDeque.size());
    sendMsg((uint8_t *) buff, strlen(buff));
}


void F50S::respondIntervalLogNum(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    sprintf(buff, "%s,%d,", cmd.c_str(), intervalLogQueue.size());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondEventRecord(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string outBuf;
    uint32_t firstCode = 0;
    uint32_t secondCode = 0;
    uint32_t thirdCode = 0;
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    string eventNumStr = data.substr(F50S_CMD_LEN, packet.len - F50S_CMD_LEN);
    uint32_t eventNum = stoi(eventNumStr);
    if(eventNum > eventDeque.size())
        eventNum = eventDeque.size();
    if(eventDeque.size() > 0) {
        EventLog eventlog = eventDeque[eventNum];

        firstCode = (uint32_t) eventlog.eventTime;
        secondCode = (uint32_t) eventlog.eventType;
        if((EventType)secondCode == EventType::AlarmOccured || (EventType)secondCode == EventType::AlarmReset)
            thirdCode = (uint32_t) eventlog.alarmCode;
        else if ((EventType)secondCode == EventType::WarningCleared || (EventType)secondCode == EventType::WarningOccured)
            thirdCode = (uint32_t) eventlog.warnCode;
        else
            thirdCode = (uint32_t) eventlog.subEventType;
    }
    std::cout << firstCode << " " << secondCode << " " << thirdCode << " " << std::endl;
    sprintf(buff, "%s%d,%d,%d,%d", cmd.c_str(), eventNum, firstCode, secondCode, thirdCode);
    outBuf.append(buff);

    sendMsg((uint8_t *) outBuf.c_str(), outBuf.size());
}


void F50S::respondIntervalLogRecord(SFPacket &packet) {
    char * buff = (char *) malloc(sizeof(char) * 500);
    string outBuf;
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    string intLogIdxStr = data.substr(F50S_CMD_LEN, packet.len - F50S_CMD_LEN);
    uint32_t intervalLogIdx = stoi(intLogIdxStr);
    cout << "intervalLogIdx: " << intervalLogIdx << std::endl;
    if (intervalLogIdx > intervalLogQueue.size())
        intervalLogIdx = intervalLogQueue.size();
    if(!intervalLogQueue.empty()) {
        IntervalLog intervalLog = intervalLogQueue.front();
        sprintf(buff, "%s%d,%d,%d,%d,%d,%d,%d,%d,%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d",
         cmd.c_str(),intervalLogIdx, (uint32_t)intervalLog.intLogTime, intervalLog.temp[0], intervalLog.temp[1], intervalLog.temp[2],
            intervalLog.temp[3],intervalLog.temp[4], intervalLog.temp[5], intervalLog.pressure[0], intervalLog.pressure[1],
            intervalLog.noUse[0], intervalLog.noUse[1], intervalLog.noUse[3], intervalLog.coldHeadInverter,
            intervalLog.controlValvePercent, intervalLog.failPredCriterion[0], intervalLog.failPredCriterion[1],
            intervalLog.failPredCriterion[2], intervalLog.failPredCriterion[3], intervalLog.failPredCriterion[4]);

        intervalLogQueue.pop();
    }
    outBuf.append(buff);
    std::cout << "OutBuf:: LIR: " << outBuf << std::endl;
    sendMsg((uint8_t *) outBuf.c_str(), outBuf.size());
    free(buff);
 }

void F50S::respondErrCRC(void) {
    static char buff[RESP_BUFF_SIZE];
    sprintf(buff, "%s,", F50SCmdStr[ErrCRC].c_str());
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondChangeFwMode(SFPacket &packet) {
    std::cout << "RespondCHange called" << std::endl;
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    if(FW_CHANGEFWMODESTATUS == 0) {
        std::cout << "Firmware update mode activated" << std::endl;
        isFwUpdateModeActivated = true;
        isFwUpdateCompleted = false;
        sprintf(buff, "%s,%d,", cmd.c_str(), FW_CHANGEFWMODESTATUS);
    } else
        sprintf(buff, "%s,","N/A");
    sendMsg((uint8_t *) buff, strlen(buff));
    std::cout << "ChangeFw: buff" << buff << "len: " << strlen(buff) << std::endl;
    std::cout << "respondChangeFwMode completed" << std::endl;
}

void F50S::respondClearProgMode(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    if(isFwUpdateModeActivated == true && !FW_CLEARPROGAREASTATUS) {
        std::cout << "Clearing Programming area ..." << std::endl;
        std::ofstream fwFileHandle;
        fwFileHandle.open(FW_FILE, std::ofstream::out | std::ofstream::trunc);
        fwFileHandle.close();
    }

    sprintf(buff, "%s,%d,", cmd.c_str(), FW_CLEARPROGAREASTATUS);

    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondWriteBlock(SFPacket &packet) {
    string packetBuf = (char *)packet.buf;
    static char buff[RESP_BUFF_SIZE];
    string cmd = packetBuf.substr(0, F50S_CMD_LEN);
    string fwBlock = packetBuf.substr(F50S_CMD_LEN, packet.len - 3);
    if(isFwUpdateModeActivated == true && !FW_WRITEFWBLKSTATUS) {
        std::ofstream fwFileHandle(FW_FILE, std::ofstream::app);
        fwFileHandle << fwBlock << std::endl;
        std::cout << "Firmware block has been written" << ": " << fwBlock << std::endl;
        fwFileHandle.close();
    }
    sprintf(buff, "%s,%d,", cmd.c_str(), FW_WRITEFWBLKSTATUS);
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::respondResetCPU(SFPacket &packet) {
    static char buff[RESP_BUFF_SIZE];
    string data = (char *)packet.buf;
    string cmd = data.substr(0, F50S_CMD_LEN);
    if(FW_RESETCPUSTATUS == 0) {
        std::cout << "CPU Reset Successfully" << std::endl;
        isFwUpdateCompleted = true;
    }
    sprintf(buff, "%s,%d,", cmd.c_str(), FW_RESETCPUSTATUS);
    sendMsg((uint8_t *) buff, strlen(buff));
}

void F50S::changeColdHeadFreq(float coldHeadFreq) {
    std::lock_guard<std::shared_timed_mutex> writeDataLock(mutexF50SData);
    pF50SData.currentCHOprFreq = coldHeadFreq;
    // Compressor action to change coldhead frequency
}

void F50S::setTemp(F50STemp tempEnum, float tempValue) {
    std::lock_guard<std::shared_timed_mutex> writeDataLock(mutexF50SData);
    pF50SData.temperature[tempEnum] = tempValue;
}

void F50S::setPres(F50SPres pressureEnum, float pressureValue) {
    std::lock_guard<std::shared_timed_mutex> writeDataLock(mutexF50SData);
    pF50SData.pressure[pressureEnum] = pressureValue;
}

void F50S::setCurrStatusCode(std::string &code) {
    std::lock_guard<std::shared_timed_mutex> writeDataLock(mutexF50SData);
    pF50SData.currentStatusCode = code;
}

void F50S::setColdHeadFreq(double freq) {
    std::lock_guard<std::shared_timed_mutex> writeDataLock(mutexF50SData);
    pF50SData.currentCHOprFreq = freq;
}

void F50S::setPresAlarmInfo(AlarmCode alarmCode) {
    std::lock_guard<std::shared_timed_mutex> writeDataLock(mutexF50SData);

    for(auto i = 0 ; i < MAX_ALARM_FROM_BEGIN + MAX_ALARM_FROM_CONT; i++) {
        if((pF50SData.alarmInfo[i] == alarmCode) ) //! Found
            return;
    }
    for(auto i = 0 ; i < MAX_ALARM_FROM_BEGIN + MAX_ALARM_FROM_CONT; i++) {
        if(pF50SData.alarmInfo[i] == AlarmCode::A000 ) {
            pF50SData.alarmInfo[i] = alarmCode;
            isAlarmCleared = false;
            std::cout << "Logging event for AlarmOccured\n";
            logEvent(EventType::AlarmOccured, SubEventType::Always,
                     alarmCode, WarnCode::W000);
            //   std::cout << "Logging event for WarnOccured\n";
            // logEvent(EventType::WarningOccured, SubEventType::Always,
            //          AlarmCode::A000, (WarnCode)alarmCode);
            break;
        }
    }

}

void F50S::setIndColdHeadOperation(bool status) {
    std::lock_guard<std::shared_timed_mutex> writeDataLock(mutexF50SData);
    pF50SData.indCHOpr = status;
}

void F50S::setF50SMode(F50SMode mode) {
    pF50SData.mF50SMode = mode;
    if(mode == F50SMode::INTERNAL ) {
        std::cout << "Logging event for Mode change to Internal\n";
        logEvent(EventType::ChangeOprModeToInternal, SubEventType::Always,
                 AlarmCode::A000, WarnCode::W000);
    } else if (mode == F50SMode::RS232) {
        std::cout << "Logging event for Mode change to External\n";
        logEvent(EventType::ChangeOprModeToExternal, SubEventType::Always,
                 AlarmCode::A000, WarnCode::W000);
    }
}

void F50S::unsetPresAlarmInfo(AlarmCode alarmCode) {
    bool found = false;
    for(auto i = 0 ; i < MAX_ALARM_FROM_BEGIN + MAX_ALARM_FROM_CONT; i++) {
        if(!found) {
            if((pF50SData.alarmInfo[i] == alarmCode)) {
                pF50SData.alarmInfo[i] = AlarmCode::A000;
        //         found = true;
        //          std::cout << "Logging event for warning cleared\n";
        // logEvent(EventType::WarningCleared, SubEventType::Always,
        //          AlarmCode::A000, (WarnCode)alarmCode);
            }
        } else {
                pF50SData.alarmInfo[i - 1] = pF50SData.alarmInfo[i];
                pF50SData.alarmInfo[i] = AlarmCode::A000;
        }
    }
}

void F50S::logEvent(EventType mEventType, SubEventType mSubeventType,
                    AlarmCode mAlarmCode, WarnCode mWarnCode) {
    EventLog eventLog;

    eventLog.eventTime = chrono::system_clock::to_time_t(chrono::system_clock::now());
    eventLog.eventType = mEventType;
    eventLog.subEventType = mSubeventType;
    eventLog.alarmCode = mAlarmCode;
    eventLog.warnCode = mWarnCode;
    std::cout << "Event Logged!\n";
    eventDeque.push_back(eventLog);
}

bool F50S::getCmprPowerStatus(void) {
    return pF50SData.powerStatus;
}

std::string F50S::getCurrStatusCode(void) {
    if(!pF50SData.currentStatusCode.empty())
        return pF50SData.currentStatusCode;
    else
        return "";
}

F50SMode F50S::getF50SMode(void) {
    return pF50SData.mF50SMode;
}

void F50S::startF50S(SubEventType subEventType) {
    //TBD:: Need to handle subEventType
    setCurrStatusCode(F50SStatusCodeStr[PrepareRun]);
    logEvent(EventType::StartRunning, subEventType,
            AlarmCode::A000, WarnCode::W000 );
}

void F50S::stopF50S(SubEventType subEventType) {
    pF50SData.powerStatus = false;
    setCurrStatusCode(F50SStatusCodeStr[PrepareStop]);
    logEvent(EventType::StopRunning, subEventType,
            AlarmCode::A000, WarnCode::W000 );
}

void F50S::toggleF50SPowerState(void) {
    if(pF50SData.powerStatus)
        stopF50S(SubEventType::OperatedByRunStopSwitch);
    else
        startF50S(SubEventType::OperatedByRunStopSwitch);
}

void F50S::resetAlarms() {
    isAlarmCleared = true;
    setCurrStatusCode(F50SStatusCodeStr[AlarmReset]);
    std::cout  << "Logging event for Reset Alarm\n";
    logEvent(EventType::AlarmReset, SubEventType::Always,
            AlarmCode::A000, WarnCode::W000 );
    for(auto i = 0 ; i < MAX_ALARM_FROM_BEGIN + MAX_ALARM_FROM_CONT; i++) {
        if(pF50SData.alarmInfo[i] != AlarmCode::A000 ) {
            isAlarmCleared = false;
            std::cout << "Alarm is present ....................." << std::endl;
            break;
        }
    }
    if(isAlarmCleared) {
        setCurrStatusCode(F50SStatusCodeStr[ProcToOprMode]);
        std::cout << "Logging event for ProcToOprMode\n";

    }
}

vector <string> splitData(const string &data, char delim) {
    vector <string> tokens;
    stringstream check1(data);

    string intermediate;

    while(getline(check1, intermediate, delim))
    {
        tokens.push_back(intermediate);
    }
    return tokens;
 }

// helper func for testing
void dumpFrame(const uint8_t *buff, size_t len)
{
    size_t i;
    for (i = 0; i < len; i++) {
        printf("%3u \033[94m%02X\033[0m", buff[i], buff[i]);
        if (buff[i] >= 0x20 && buff[i] < 127) {
            printf(" %c", buff[i]);
        }
        else {
            printf(" \033[31m.\033[0m");
        }
        printf("\n");
    }
    printf("--- end of frame ---\n\n");
}

void dumpFrameInfo(SFPacket &packet)
{
    printf("\033[33mFrame info\n"
               "  data: \"%.*s\"\n"
               "   len: %u\033[0m\n\n",
           packet.len, packet.buf, packet.len);
}
