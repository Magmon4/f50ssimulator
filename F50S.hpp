#ifndef F50S_HPP
#define F50S_HPP
#include "F50SConstants.h"
#include <stdio.h>
#include <string.h>
#include <vector>
#include "F50SIFace/singleton.hpp"
#include <unistd.h>
#include <thread>
#include <atomic>
#include <bits/stdc++.h>
#include <set>
#include "F50SIFace/UARTIface.hpp"
#include <mutex>
#include <shared_mutex>
#include "F50SIFace/SerialFrame.hpp"
#include "libcrc/checksum.h"

#define F50S_CMD_LEN            3
#define TEMP_MAX                6
#define PRESSURE_MAX            2
#define CCN_SIZE                10
#define STATUS_CODE_SIZE        10
#define MAX_ALARM               49
#define MAX_ALARM_FROM_BEGIN    10
#define MAX_ALARM_FROM_CONT     10
#define FAIL_PRED_MAX           5
#define RESP_BUFF_SIZE          500
#define SOFT_VERSION            "1.00"
#define CHECK_INTERVAL          1
#define CONF_FILE               "/mm4data/F50SConfig.cfg"
#define RESPONSE_TIMEOUT        5000 //! In Milliseconds
#define UART_DEV_FILE           "/dev/ttyUSB0"
#define STATUS_CHANGE_INTERVAL  1
#define SIMULATE_CRC_MISMATCH   0

#define FW_CHANGEFWMODESTATUS   0
#define FW_CLEARPROGAREASTATUS  0
#define FW_WRITEFWBLKSTATUS     0
#define FW_RESETCPUSTATUS       0

#define FW_FILE             "/tmp/fwFileF50S.mot"

using namespace std;

/** pointer to unsigned char */
typedef unsigned char* pu8;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Dump a binary frame as hex, dec and ASCII
 */
void dumpFrame(const uint8_t *buff, size_t len);

/**
 * Dump message metadata (not the content)
 *
 * @param msg
 */
void dumpFrameInfo(SFPacket &packet);

#ifdef __cplusplus
}
#endif

std::vector <std::string> splitData(const std::string &data, char delim);


/**
 * @brief Event Log structure holding event data responsed from Event
 *  Log Commands
 *
 */

typedef struct EventLog {
    std::time_t eventTime;
    EventType eventType;
    SubEventType subEventType;
    AlarmCode alarmCode;
    WarnCode warnCode;
};

typedef struct IntervalLog {
    std::time_t intLogTime;
    int temp[TEMP_MAX];
    int pressure[PRESSURE_MAX];
    int noUse[3];
    int coldHeadInverter;
    int controlValvePercent;
    int failPredCriterion[FAIL_PRED_MAX];
};

typedef struct F50SData {
    float temperature[TEMP_MAX];          //! All Temperature Values (T1 to T6)
    float pressure[PRESSURE_MAX];         //!All Pressure Values (P1 to P2)
    float totalRunTime;
    float adbrUsageTime;
    float adbrRemainderTime;
    float currentCHOprFreq;
    float serviceTimer;
    float failPredCriterion[FAIL_PRED_MAX];

    char  ccn[CCN_SIZE];                  //! System Structural File Classification
    int   cal;                            //! Abnormal Stop (Alarm OFF) History (10 Cases)
    int   cwa;                            //! Warning History (10 Cases)
    bool  IsCmprSOn;                //! Compressor Turn ON:: (0 - OFF , 1- ON)
    bool  indCHOpr;                     //! Individual Cold Head Operation (0 - OFF , 1- ON)
    bool  powerStatus;
    std::string softVersion;
    std::string currentStatusCode;
    AlarmCode alarmInfo[MAX_ALARM_FROM_BEGIN + MAX_ALARM_FROM_CONT];
    uint32_t eventLogNum;
    F50SMode mF50SMode;
};

class F50S {

public:

using Seconds = std::chrono::duration<double, std::chrono::seconds::period>;

    F50S() : mThread(&F50S::rxThread, this),
    mCheckThread(&F50S::CheckThread, this){
        Initialization();
    }
    ~F50S() {
        mKillRxThread = true;
        mThread.join();
        mKillCheckThread = true;
        mCheckThread.join();
    }

    std::vector <std::string> F50SCmdStr = {
        "MTA",
        "MT1", "MT2", "MT3", "MT4", "MT5", "MT6",
        "MPA",
        "MP1", "MP2",
        "CTT", "CAT", "CAR",
        "CSW", "CCN", "CAL", "CWA",
        "OON", "OFF", "ORS", "OCN", "OCF",
        "ICS", "ICH", "CLT", "CSI",
        "CAS", "CAN",
        "CF1", "CF2", "CF3", "CF4", "CF5",
        "CMO", "OCE",
        "LEN", "LER", "LIN", "LIR",
        "???",
        "FST", "FER", "FDT", "FED"
    } ;

    std::string F50SStatusCodeStr[Max_Status] = {
        "00", "10", "20", "30", "40", "50",
        "60", "70", "80", "90", "100", "110", "140"
    } ;


    void Initialization(void);

    void handleRxPacket(SFPacket &packet);
    //! Controls
    void startF50S(SubEventType subEventType);
    void stopF50S(SubEventType subEventType);
    void toggleF50SPowerState(void);
    void resetSCmpr(void);
    void changeColdHeadFreq(float coldHeadFreq);
    bool getCmprPowerStatus(void);

    //! Data
    void getAllTemp(float *temperature);
    float getHeTempPreH20(void) { return pF50SData.temperature[eCmprSHeTempPreH20]; }
    float getHeTempAftH20(void) { return pF50SData.temperature[eCmprSHeTempAftH20]; }
    float getOilTempPreH20(void) { return pF50SData.temperature[eCmprSOilTempPreH20]; }
    float getOilTempAftH20(void) { return pF50SData.temperature[eCmprSOilTempAftH20]; }
    float getWaterTempInlet(void) { return pF50SData.temperature[eCmprsSWaterTempInlet]; }
    float getWaterTempOutlet(void) { return pF50SData.temperature[eCmprsSWaterTempOutlet]; }

    float getSupplyPressure(void) { return pF50SData.pressure[eCmprSHePresSupply]; }
    float getReturnPressure(void) { return pF50SData.pressure[eCmprSHePresReturn]; }
    float getColdHeadFreq(void) {
        std::shared_lock<std::shared_timed_mutex> readDataLock(mutexF50SData);
        return pF50SData.currentCHOprFreq;
    }
    std::string getSoftVersion(void);
    AlarmCode getAlarmInfo(int index);
    float getTotalRunTime(void) { return pF50SData.totalRunTime; }
    bool getIndColdHeadOperation(void) { return pF50SData.indCHOpr; }
    void setTemp(F50STemp tempEnum, float tempValue);
    void setHeTempPreH20(float temp);
    void setHeTempAftH20(float temp);
    void setOilTempPreH20(float temp);
    void setOilTempAftH20(float temp);
    void setWaterTempInlet(float temp);
    void setWaterTempOutlet(float temp);
    void setPres(F50SPres pressureEnum, float pressureValue);
    void setPresAlarmInfo(AlarmCode alarmCode);
    void unsetPresAlarmInfo(AlarmCode code);
    void setIndColdHeadOperation(bool status);
    void setF50SMode(F50SMode mode);
    void resetAlarms(void);

    void logEvent(EventType mEventType,
                  SubEventType mSubeventType,
                  AlarmCode mAlarmCode,
                  WarnCode mWarnCode);
    //! STATUS
    F50SMode getF50SMode(void);
    std::string getCurrStatusCode(void);
    void setCurrStatusCode(std::string &code);
    void setColdHeadFreq(double freq);

    void sendMsg(const uint8_t* sendBuf, size_t sendBuflen) {
        size_t outlen;
        sfWritePacket(&mContext, sendBuf, sendBuflen, &outlen);
    }

    void acceptFrame(uint8_t * octets, size_t len) {
        for (auto i = 0; i < len; i++) {
            try {
                sfDeliverOctet(&mContext, octets[i], nullptr, 0, nullptr, SIMULATE_CRC_MISMATCH);
            } catch (CRCMismatch) {
                throw CRCMismatch();
            }
        }
    }


private:
    void rxThread();
    void CheckThread();
    void respondAllTemp(SFPacket &packet);
    void respondHeTempPreH2O(SFPacket &packet);
    void respondHeTempAftH2O(SFPacket &packet);
    void respondOilTempPreH2O(SFPacket &packet);
    void respondOilTempAftH2O(SFPacket &packet);
    void respondWaterTempInlet(SFPacket &packet);
    void respondWaterTempOutlet(SFPacket &packet);
    void respondAllPres(SFPacket &packet);
    void respondCurStatusCode(SFPacket &packet);
    void respondTurnOnF50S(SFPacket &packet);
    void respondTurnOffF50S(SFPacket &packet);
    void respondPresAlarmInfoBegin(SFPacket &packet);
    void respondPresAlarmInfoCont(SFPacket &packet);
    void respondF50SMode(F50SMode mode);
    void respondColdHeadFreq(std::vector <std::string> &token, SFPacket &packet);
    void respondSoftVersion(SFPacket &packet);
    void respondSysStructFileClass(SFPacket &packet);
    void respondCurrCHOprFreq(SFPacket &packet);
    void respondSetCHOprFreq(SFPacket &packet);
    void respondTotalRunTime(SFPacket &packet);
    void respondAdsorberUsageTime(SFPacket &packet);
    void respondAdsorberRemainderTime(SFPacket &packet);
    void respondServiceTimer(SFPacket &packet);
    void respondAbnormalStopHistory(SFPacket &packet);
    void respondWarningHistory(SFPacket &packet);
    void respondOnIndCHOpr(SFPacket &packet);
    void respondOffIndCHOpr(SFPacket &packet);
    void respondF50SMode(SFPacket &packet);
    void respondResetAlarm(SFPacket &packet);
    void respondEventLogNum(SFPacket &packet);
    void respondEventRecord(SFPacket &packet);
    void respondIntervalLogNum(SFPacket &packet);
    void respondIntervalLogRecord(SFPacket &packet);
    void respondFailurePredCriterion_1(SFPacket &packet);
    void respondFailurePredCriterion_2(SFPacket &packet);
    void respondFailurePredCriterion_3(SFPacket &packet);
    void respondFailurePredCriterion_4(SFPacket &packet);
    void respondFailurePredCriterion_5(SFPacket &packet);
    void respondChangeModeToExt(SFPacket &packet);

    void respondErrCRC(void);

    void respondChangeFwMode(SFPacket &packet);
    void respondClearProgMode(SFPacket &packet);
    void respondWriteBlock(SFPacket &packet);
    void respondResetCPU(SFPacket &packet);

    Seconds tickInterval () const {
            return Seconds {CHECK_INTERVAL};
    }

    static void staticDeliver(uint8_t * buf, size_t len, void * data) {
        SFPacket packet;
        memcpy(packet.buf, buf, len);
        packet.len = len;
//        dumpFrame(packet.buf, packet.len);
//        std::cout << "Delivered" << std::endl;
        static_cast<F50S*>(data)->handleRxPacket(packet);
    }

    static int staticWriteToUART(uint8_t *octets, size_t len, size_t outlen, void *data) {
//        dumpFrame(octets, len);
        UARTIfaceSingleton::getInstance()->writePort(octets, len);
    }

    static void staticLock (void *data) {
        static_cast<F50S*>(data)->mTxMutex.lock();
//        std::cout << "Locked!" << std::endl;
    }
    static void staticUnlock (void *data) {
        static_cast<F50S*>(data)->mTxMutex.unlock();
//        std::cout << "UnLocked!" << std::endl;
    }

    F50SData pF50SData;
    std::atomic <bool> mKillRxThread = { false };
    std::atomic <bool> mKillCheckThread = { false };
    std::thread mThread;
    std::thread mCheckThread;
    DataBuffer recvDataBuff;
    char buff[RESP_BUFF_SIZE];
    std::shared_timed_mutex mutexF50SData;
    std::deque <EventLog> eventDeque;
    std::queue <IntervalLog> intervalLogQueue;

    SFcontext mContext;
    std::mutex mTxMutex;
    bool isAlarmCleared = true;
    bool isFwUpdateModeActivated = false;
    bool isFwUpdateCompleted = false;

};

typedef Singleton<F50S> F50SSingleton;
#endif // F50S_HPP

