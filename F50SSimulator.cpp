#include "F50SSimulator.h"
#include "ui_F50SSimulator.h"
#include <QMessageBox>

const AlarmCode F50SAlarmCode[MAX_ALARM] = {
    AlarmCode::A001, AlarmCode::A002, AlarmCode::A003, AlarmCode::A004, AlarmCode::A006, AlarmCode::A007, AlarmCode::A008, AlarmCode::A009,
    AlarmCode::A101, AlarmCode::A102, AlarmCode::A103, AlarmCode::A104, AlarmCode::A106, AlarmCode::A107, AlarmCode::A108, AlarmCode::A109,
    AlarmCode::A116, AlarmCode::A117, AlarmCode::A118, AlarmCode::A119, AlarmCode::A121, AlarmCode::A122, AlarmCode::A123, AlarmCode::A124,
    AlarmCode::A146, AlarmCode::A147, AlarmCode::A148, AlarmCode::A149, AlarmCode::A151, AlarmCode::A152, AlarmCode::A153, AlarmCode::A154,
    AlarmCode::A165, AlarmCode::A601, AlarmCode::A603, AlarmCode::A605, AlarmCode::A300, AlarmCode::A306, AlarmCode::A307, AlarmCode::A308,
    AlarmCode::A309, AlarmCode::A310, AlarmCode::A311, AlarmCode::A314, AlarmCode::A315, AlarmCode::A316, AlarmCode::A317, AlarmCode::A318,
    AlarmCode::A319
};


F50SSimulator::F50SSimulator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::F50SSimulator), mUpdateUIThread(&F50SSimulator::updateUIThread, this)
{
    ui->setupUi(this);    
    ui->F50SMode->addItem("Internal");
    ui->F50SMode->addItem("D_Sub15");
    ui->F50SMode->addItem("RS232");

    for(auto i = 0; i < MAX_ALARM; i++) {
        ui->addAlarm->addItem(QString::number((int)F50SAlarmCode[i]));
        ui->delAlarm->addItem(QString::number((int)F50SAlarmCode[i]));
    }
    F50SSingleton::getInstance();
    IsConfigured = true;
}

F50SSimulator::~F50SSimulator()
{
    delete ui;
    killUpdateUIThread = true;
    mUpdateUIThread.join();
}

void F50SSimulator::updateUIThread(void) {
//    for(auto i = 0; i < MAX_ALARM_FROM_BEGIN; i++) {
//        F50SSingleton::getInstance()->setPresAlarmInfo(AlarmCode::A000);
//    }

    while(!killUpdateUIThread) {
        if(IsConfigured) {
            std::string statusCode = F50SSingleton::getInstance()->getCurrStatusCode();
            ui->CurrentStatusCode->setText(QString::fromStdString(statusCode));

            ui->Err1B->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(0)));
            ui->Err2B->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(1)));
            ui->Err3B->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(2)));
            ui->Err4B->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(3)));
            ui->Err5B->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(4)));
            ui->Err6B->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(5)));
            ui->Err7B->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(6)));
            ui->Err8B->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(7)));
            ui->Err9B->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(8)));
            ui->Err10B->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(9)));

            ui->Err1C->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(10)));
            ui->Err2C->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(11)));
            ui->Err3C->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(12)));
            ui->Err4C->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(13)));
            ui->Err5C->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(14)));
            ui->Err6C->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(15)));
            ui->Err7C->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(16)));
            ui->Err8C->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(17)));
            ui->Err9C->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(18)));
            ui->Err10C->setText(QString::number((int)F50SSingleton::getInstance()->getAlarmInfo(19)));

            ui->CurrCHFreq->setText(QString::number(F50SSingleton::getInstance()->getColdHeadFreq()));
            ui->totalRunTime->setText(QString::number(F50SSingleton::getInstance()->getTotalRunTime()));
            ui->IndCHOpr->setText(QString::number(F50SSingleton::getInstance()->getIndColdHeadOperation()));
        }
        usleep(100);
    }
}

void F50SSimulator::on_HeTempPreH20_valueChanged(double arg1)
{
    F50SSingleton::getInstance()->setTemp(eCmprSHeTempPreH20, arg1);
}

void F50SSimulator::on_HeTempAftH20_valueChanged(double arg1)
{
    F50SSingleton::getInstance()->setTemp(eCmprSHeTempAftH20, arg1);

}

void F50SSimulator::on_OilTempPreH20_valueChanged(double arg1)
{
    F50SSingleton::getInstance()->setTemp(eCmprSOilTempPreH20, arg1);

}

void F50SSimulator::on_OilTempAftH20_valueChanged(double arg1)
{
    F50SSingleton::getInstance()->setTemp(eCmprSOilTempAftH20, arg1);

}

void F50SSimulator::on_WaterTempInlet_valueChanged(double arg1)
{
    F50SSingleton::getInstance()->setTemp(eCmprsSWaterTempInlet, arg1);

}

void F50SSimulator::on_WaterTempOutlet_valueChanged(double arg1)
{
    F50SSingleton::getInstance()->setTemp(eCmprsSWaterTempOutlet, arg1);
}

void F50SSimulator::on_HePresSupply_valueChanged(double arg1)
{
    F50SSingleton::getInstance()->setPres(eCmprSHePresSupply, arg1);
}

void F50SSimulator::on_HePresReturn_valueChanged(double arg1)
{
    F50SSingleton::getInstance()->setPres(eCmprSHePresReturn, arg1);
}

void F50SSimulator::on_CurrentCHFreq_valueChanged(double arg1)
{
    F50SSingleton::getInstance()->setColdHeadFreq(arg1);
}

void F50SSimulator::on_F50SMode_activated(int index)
{
    F50SSingleton::getInstance()->setF50SMode((F50SMode)(index + 1));
}

void F50SSimulator::on_addAlarm_activated(const QString &arg1)
{
    F50SSingleton::getInstance()->setPresAlarmInfo((AlarmCode)arg1.toInt());
}

void F50SSimulator::on_delAlarm_activated(const QString &arg1)
{
    F50SSingleton::getInstance()->unsetPresAlarmInfo((AlarmCode)arg1.toInt());
}

void F50SSimulator::on_pushButton_clicked(bool checked)
{
    F50SSingleton::getInstance()->toggleF50SPowerState();
}


