#-------------------------------------------------
#
# Project created by QtCreator 2018-10-03T12:51:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = F50SSimulator
TEMPLATE = app
CONFIG += c++14
QT += widgets
SOURCES += main.cpp\
        F50SSimulator.cpp \
    F50S.cpp

HEADERS  += F50SSimulator.h \
    F50S.hpp \
    F50SConstants.h

FORMS    += F50SSimulator.ui
unix:!macx: LIBS += -lserial
unix:!macx: LIBS += -lF50SIFace

unix:!macx: LIBS += -lcrc


